require 'rails_helper'

RSpec.describe CinemasController, type: :controller do


	context 'Creating a movie route' do 
		it 'creates a movie method in the CinemasController which scraps all movies' do
		
		def movie
			require 'open-uri'

  	 		doc = Nokogiri::HTML(open('https://silverbirdcinemas.com/cinema/accra/', 'User-Agent' => 'Nooby', :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))
 	 		@entries = doc.css('.entry-item')
  			@entriesArray = []

 			 @entries.each do |entry|
    
   			 title = entry.css('.entry-title').text
      		 link = entry.css('.entry-title>a')[0]['href']
     		 duration = entry.css('.entry-title').text
     		 synopsis = entry.css('.entry-title').text
   			 image = entry.css('.entry-thumb img').attr('src')
   			 @entriesArray << Cinema.new( title, link,duration,synopsis,image)

 		 end
		end
	end
	end



	context 'Creating a get_movies to fetch all movie details route' do 
		it 'creates a movie method in the CinemasController which scraps all movies' do
		
		def get_movies
		 	require 'open-uri'

 			 url =params[:id]
  			 doc = Nokogiri::HTML(open(url, 'User-Agent' => 'Nooby', :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))

 
  			 @entries = doc.css('.entry-info')
 			 @entriesArray = []
   			 
  			 @entries.each do |entry|

   			 title = entry.css('.info-list').text
    		 link = entry.css('.entry-title').text
             duration = entry.css('.entry-pg').text
             synopsis =  entry.xpath('//span[@style]').text
             image = doc.css('.entry-thumb img').attr('src')   
 
  			 @entriesArray << Cinema.new(title , link, duration,synopsis,image)

 
 			 end
		end
		
end	
end

end
