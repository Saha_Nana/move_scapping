class CinemasController < ApplicationController
  before_action :set_cinema, only: [:show, :edit, :update, :destroy]

  # GET /cinemas
  # GET /cinemas.json
  def index
    @cinemas = Cinema.all
  end


###################  Creating a class for Cinema ###########################
   class Cinema
    def initialize(title, link, duration,synopsis,image)
      @title = title
      @link = link
      @duration = duration
      @synopsis = synopsis
      @image = image
    end
    attr_reader :title
    attr_reader :link
    attr_reader :duration
    attr_reader :synopsis
    attr_reader :image
  end




def movies
  # Pull in the page
  require 'open-uri'

  doc = Nokogiri::HTML(open('https://silverbirdcinemas.com/cinema/accra/', 'User-Agent' => 'Nooby', :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))

  # Narrow down on what we want and build the entries array
  @entries = doc.css('.entry-item')
  logger.info @entries
  @entriesArray = []


  @entries.each do |entry|

    #### Getting the movie titles and link from website and oops images too
    title = entry.css('.entry-title').text
    link = entry.css('.entry-title>a')[0]['href']
    duration = entry.css('.entry-title').text  #not using duration for this particular one
    synopsis = entry.css('.entry-title').text   #not using synopsis for this particular one
    image = entry.css('.entry-thumb img').attr('src')

    @entriesArray << Cinema.new( title, link,duration,synopsis,image)

    logger.info @entriesArray
  end

end


def get_movies
   require 'open-uri'

  url =params[:id]   ## Assumiing we fetching the id from each movie .in this case the id is the href link the website provides
  doc = Nokogiri::HTML(open(url, 'User-Agent' => 'Nooby', :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))

  # Narrow down on what we want and build the entries array
  @entries = doc.css('.entry-info')  ## Entries contains a host of pages classes we can select from
  @entriesArray = []
  
  @entries.each do |entry|

    title = entry.css('.info-list').text
    link = entry.css('.entry-title').text
    duration = entry.css('.entry-pg').text
    synopsis =  entry.xpath('//span[@style]').text
    image = doc.css('.entry-thumb img').attr('src')   
   
    @entriesArray << Cinema.new(title , link, duration,synopsis,image)
  end

end






  # GET /cinemas/1
  # GET /cinemas/1.json
  def show
  end

  # GET /cinemas/new
  def new
    @cinema = Cinema.new
  end

  # GET /cinemas/1/edit
  def edit
  end

  # POST /cinemas
  # POST /cinemas.json
  def create
    @cinema = Cinema.new(cinema_params)

    respond_to do |format|
      if @cinema.save
        format.html { redirect_to @cinema, notice: 'Cinema was successfully created.' }
        format.json { render :show, status: :created, location: @cinema }
      else
        format.html { render :new }
        format.json { render json: @cinema.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cinemas/1
  # PATCH/PUT /cinemas/1.json
  def update
    respond_to do |format|
      if @cinema.update(cinema_params)
        format.html { redirect_to @cinema, notice: 'Cinema was successfully updated.' }
        format.json { render :show, status: :ok, location: @cinema }
      else
        format.html { render :edit }
        format.json { render json: @cinema.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cinemas/1
  # DELETE /cinemas/1.json
  def destroy
    @cinema.destroy
    respond_to do |format|
      format.html { redirect_to cinemas_url, notice: 'Cinema was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cinema
      @cinema = Cinema.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cinema_params
      # params.fetch(:cinema, {})

       params.require(:cinema).permit(:title,:link)
    end
end
